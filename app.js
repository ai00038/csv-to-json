const fs = require('fs');
const parseCSVToJson = require('./parse-csv-to-json');

let parsedJson = null;

const onRead =  (err, data) => {
   if (err) {
      return console.error(err);
   }
   parsedJson = parseCSVToJson(data.toString());

   writeToJsonFile(parsedJson, 'superstore-sales.json');
};

const onWrite = (err) => {
  if (err) { 
    console.log('Error: ', err);
    return;
  }

  console.log('File uploaded successfully'); 
}

const writeToJsonFile = (jsonData, fileName) => {
    let jsonString = JSON.stringify(jsonData);
    fs.writeFile(fileName, jsonString, onWrite);
}

fs.readFile('superstore-sales--edit.csv', onRead);