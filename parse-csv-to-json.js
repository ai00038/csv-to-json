const parseCSVToJson = (csvString) => {
    let csvRows = csvString.split('\r\n').map((row) => {
        return row.split(',');
    });

    const columnHeaders = csvRows[0];

    let JSONArray = [];

    for(let i = 1; i < csvRows.length; i++) {
        JSONArray.push(parseColumns(csvRows[i], columnHeaders));
    }

    return JSONArray;
};

const parseColumns = (csvRow, columnHeaders) => {
    let obj = {};

    columnHeaders.forEach((header, index) => {
        obj[header] = csvRow[index];
    });

    return obj;
}

module.exports = parseCSVToJson;